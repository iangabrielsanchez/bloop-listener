using StackOverflow;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Listener_v0._01
{
    public class NetworkInformation
    {
        public string ipAddress;
        public string macAddress;
        public string networkAddress;
        public string gatewayAddress;
        public NetworkInterfaceType InterfaceType { get; }
        public string[] Hosts { get; }

        public NetworkInformation(NetworkInterfaceType interfaceType)
        {
            InterfaceType = interfaceType;
            var networkInformation = GetNetworkInformation(interfaceType);
            IPSegment localSegment = new IPSegment(
            "IPAddress".GetValue(networkInformation),
            "SubnetMask".GetValue(networkInformation)
            );
            Hosts = GetHosts(localSegment);
            ipAddress = "IPAddress".GetValue(networkInformation);
            gatewayAddress = "GatewayAddress".GetValue(networkInformation);
            macAddress = "MACAddress".GetValue(networkInformation);
            networkAddress = localSegment.NetworkAddress.ToIpString();

        }


        private string[] GetHosts(IPSegment ip)
        {
            List<string> x = new List<string>();
            foreach (var host in ip.Hosts())
            {
                x.Add(host.ToIpString());
            }
            return x.ToArray();
        }

        private Dictionary<string, string> GetNetworkInformation(NetworkInterfaceType interfaceType)
        {
            var netInfo = new Dictionary<string, string>();
            IPHostEntry hostEntry = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress[] ipAddresses = hostEntry.AddressList;

            IPAddress address = System.Net.IPAddress.None;
            //PhysicalAddress macAddress = PhysicalAddress.None;

            foreach (var ipAddress in ipAddresses)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    address = ipAddress;
                    netInfo.Add("IPAddress", address.ToString());
                }
            }

            foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (adapter.NetworkInterfaceType == interfaceType)
                {
                    foreach (UnicastIPAddressInformation unicastIPAddressInformation in adapter.GetIPProperties().UnicastAddresses)
                    {
                        if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            if (address.Equals(unicastIPAddressInformation.Address))
                            {
                                //macAddress = adapter.GetPhysicalAddress();
                                netInfo.Add("SubnetMask", unicastIPAddressInformation.IPv4Mask.ToString());
                                netInfo.Add("MACAddress", adapter.GetPhysicalAddress().ToString());
                                netInfo.Add("NetworkPrefix", unicastIPAddressInformation.PrefixLength.ToString());
                                netInfo.Add("NetworkSpeed", adapter.Speed.ToString());
                                netInfo.Add("GatewayAddress",
                                adapter.GetIPProperties().GatewayAddresses.FirstOrDefault().Address.ToString());
                            }
                        }
                    }
                }

            }
            return netInfo;
        }
    }
}