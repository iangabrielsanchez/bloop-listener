﻿using System.Collections.Generic;

namespace Listener_v0._01
{
    static class DictionaryHelper
    {
        public static string GetValue(this string key, Dictionary<string, string> dictionary)
        {
            string result;
            dictionary.TryGetValue(key, out result);
            return result;
        }
    }
}
