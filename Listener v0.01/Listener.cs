﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

namespace Listener_v0._01
{
    class Listener
    {
        private int port;
        private NetworkInformation networkInformation;
        private Thread _peerDiscoveryThread;
        public bool IsServer = false;
        public bool IsConnectedToServer = false;
        private bool resolveNames = true;
        private System.Timers.Timer serverCheck = new System.Timers.Timer();

        public Listener(int port)
        {
            this.port = port;
            networkInformation = new NetworkInformation(NetworkInterfaceType.Wireless80211);
            //_peerDiscoveryThread = new Thread(DiscoverPeers);
            serverCheck.AutoReset = true;
            serverCheck.Interval = 15000;
            serverCheck.Elapsed += ServerCheck_Elapsed;
            //_peerDiscoveryThread.Start();
            serverCheck.Start();

        }

        private void ServerCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("Starting peer discovery");
            DiscoverPeers();
        }

        private void DiscoverPeers()
        {
            //serverCheck.Stop();
            string[] hosts = networkInformation.Hosts;
            int timeout = 5000;

            foreach (var host in hosts)
            {
                var ping = new Ping();
                ping.PingCompleted += PingCompleted;
                //Console.WriteLine("PINGING {0}", host);
                ping.SendAsync(host, timeout, host);
            }
            Console.WriteLine("Finished Pinging. Starting servercheck countdown");
            serverCheck.Start();
            //_peerDiscoveryThread.Abort();
        }

        private void PingCompleted(object sender, PingCompletedEventArgs e)
        {
            string host = (string)e.UserState;
            if (e.Reply != null && e.Reply.Status == IPStatus.Success)
            {
                if (resolveNames)
                {
                    string name;
                    try
                    {
                        IPHostEntry hostEntry = Dns.GetHostEntry(host);
                        name = hostEntry.HostName;
                    }
                    catch (SocketException ex)
                    {
                        name = "?";
                    }
                    Console.WriteLine("{0} ({1}) is up: ({2} ms)", host, name, e.Reply.RoundtripTime);
                }
                else
                {
                    Console.WriteLine("{0} is up: ({1} ms)", host, e.Reply.RoundtripTime);
                }
            }
            else if (e.Reply == null)
            {
                Console.WriteLine("Pinging {0} failed. (Null Reply object?)", host);
            }
        }
    }
}
